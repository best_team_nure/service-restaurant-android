# Project Title
Service "Restaurant"

## Description

This project is being developed for the restaurant to facilitate its activities like 
creating and processing orders by guest, cotrolling table's status, which are located 
in this restaurant, booking tables for concrete time and date.

### Created by: ###

* [Radchenko Nikita](https://bitbucket.org/NikitaRadchenko/)
* [Lutsenko Vladislav](https://bitbucket.org/vladislav_lut/)
* [Leshkevych Anastasiia](https://bitbucket.org/anastasiia_leshkevych/)
* [Dzhmil Ihor](https://bitbucket.org/igor26/)
* [Feoklistov Vadim](https://bitbucket.org/VadyaZombie/)
